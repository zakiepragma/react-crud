import { Button, Checkbox, Form } from "semantic-ui-react";
import React, { useState } from "react";

export default function Create() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [checkbox, setCheckbox] = useState("");

  const postdata = () => {
    console.log(firstName);
    console.log(lastName);
    console.log(checkbox);
  };
  return (
    <>
      <Form className="create-form">
        <Form.Field>
          <label for="firstname">First Name</label>
          <input
            type="text"
            name="firstname"
            value=""
            onChange={(e) => setFirstName(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label for="lastname">Last Name</label>
          <input
            type="text"
            name="lastname"
            value=""
            onChange={(e) => setLastName(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <Checkbox
            label="I agree to the Terms and Conditions"
            onChange={(e) => setCheckbox(e.target.value)}
          />
        </Form.Field>
        <Button onClick={postdata} type="submit">
          Submit
        </Button>
      </Form>
    </>
  );
}
