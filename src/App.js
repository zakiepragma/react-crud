import logo from "./logo.svg";
import "./App.css";
import Create from "./components/create";

function App() {
  return (
    <div className="main">
      <h2 className="main-header">Operasi CRUD React-JS</h2>
      <div>
        <Create />
      </div>
    </div>
  );
}

export default App;
